# Practice:
- **UIKit**

+ MKLocalSearch and Request for MapKit
+ Display pins using custom MKAnnotations
+ Look up routes using MKDirections and render lines using MKPolylineRenderer
+ Integrate Google Places SDK to show local places
+ Quickly prototype using SwiftUI Previews


# Screenshoots:

User Location |          Map Searching
:-------------------------:|:-------------------------:
![Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.22.47](/uploads/04a7ccf026204819eb99cf77f2f3c161/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.22.47.png) | ![Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.28.28](/uploads/5c41d1f5af3647533b19c9838f83a762/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.28.28.png)

Directions with Polylines  |          Routing Information Display
:-------------------------:|:-------------------------:
![Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.30.35](/uploads/e191e6c5875325900ab70a1b9f04db4a/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.30.35.png) | ![Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.30.45](/uploads/8a9b4cbd8cbc4677353585c9a52920a2/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.30.45.png)

Google Places SDK Integration  |          Detailed Photos List
:-------------------------:|:-------------------------:
![Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.32.17](/uploads/8283f3cd70e517454c396e98b9a4b704/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.32.17.png) | ![Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.32.39](/uploads/54bf4d0a35b8306971aaf5b0215350a2/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-02-17_at_10.32.39.png)
