//
//  ContentView.swift
//  MapsDirectionsGooglePlaces
//
//  Created by Tri Nguyen on 16/01/2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
