//
//  RouteHeader.swift
//  MapsDirectionsGooglePlaces
//
//  Created by Tri Nguyen on 01/02/2021.
//

import SwiftUI
import MapKit

class RouteHeader: UICollectionReusableView {
    
    let nameLabel = UILabel(text: "Route Name", font: .systemFont(ofSize: 16))
    let distanceLabel = UILabel(text: "Distance", font: .systemFont(ofSize: 16))
    let estimatedTimeLabel = UILabel(text: "Estimated Time", font: .systemFont(ofSize: 16))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        hstack(stack(nameLabel, distanceLabel, estimatedTimeLabel, spacing: 8),
               alignment: .center
        ).withMargins(.allSides(16))
        
        nameLabel.attributedText = generateAttributedString(title: "Route", description: "US-101 S")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupHeaderInformation(route: MKRoute) {
        nameLabel.attributedText = generateAttributedString(title: "Route", description: route.name)
        
        let distanceKilometres = route.distance * 0.001
        let distanceString = String(format: "%2.f km", distanceKilometres)
        
        distanceLabel.attributedText = generateAttributedString(title: "Distance", description: distanceString)
        
        var timeString = ""
        if route.expectedTravelTime > 3600 {
            let h = Int(route.expectedTravelTime / 60 / 60)
            let m = Int((route.expectedTravelTime.truncatingRemainder(dividingBy: 60 * 60)) / 60 )
            timeString = String(format: "%d hr %d min", h, m)
        } else {
            let time = Int(route.expectedTravelTime / 60)
            timeString = String(format: "%d min", time)
        }
        estimatedTimeLabel.attributedText = generateAttributedString(title: "Estimated Time", description: timeString)
    }
    
    fileprivate func generateAttributedString(title: String, description: String) -> NSAttributedString {
        let attributtedString = NSMutableAttributedString(string: title + ": ", attributes: [.font: UIFont.boldSystemFont(ofSize: 16)])
        
        attributtedString.append(.init(string: description, attributes: [.font: UIFont.systemFont(ofSize: 16)]))
        
        return attributtedString
    }
    
}

//struct RouteHeader: View {
//    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
//    }
//}

struct RouteHeader_Previews: PreviewProvider {
    static var previews: some View {
        Container()
    }
    
    struct Container: UIViewRepresentable {
        func makeUIView(context: Context) -> some UIView {
            return RouteHeader()
        }
        func updateUIView(_ uiView: UIViewType, context: Context) {
            
        }
    }
}
