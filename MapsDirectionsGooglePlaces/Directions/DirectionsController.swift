//
//  DirectionsController.swift
//  MapsDirectionsGooglePlaces
//
//  Created by Tri Nguyen on 28/01/2021.
//

import UIKit
import LBTATools
import MapKit
import SwiftUI
import JGProgressHUD

class DirectionsController: UIViewController {
    
    let mapView = MKMapView()
    let navBar = UIView(backgroundColor: #colorLiteral(red: 0, green: 0.5192101002, blue: 0.9974190593, alpha: 1))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(mapView)
        mapView.delegate = self
        
        setupRegionForMap()
        
        setupNavBar()
        
        mapView.anchor(top: navBar.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        mapView.showsUserLocation = true
        
        setupRouteButton()
        
//        setupStartEndDummyAnnotations()
        
//        requestForDirections()
        
        navigationController?.navigationBar.isHidden = true
    }
    
    fileprivate func setupRouteButton() {
        let button = UIButton(title: "Route", titleColor: .black, font: .boldSystemFont(ofSize: 16), backgroundColor: .white, target: self, action: #selector(handleRoute))
        
        view.addSubview(button)
        button.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, padding: .allSides(16), size: .init(width: 0, height: 50))
    }
    
    @objc fileprivate func handleRoute() {
        let vc = RoutesController()
        vc.route = currentlyShowingRoute
        vc.items = currentlyShowingRoute?.steps.filter{!$0.instructions.isEmpty} ?? []
        
        navigationController?.present(vc, animated: true)
    }
    
    class RoutesStepCell: LBTAListCell<MKRoute.Step> {
        
        var nameLabel = UILabel(text: "Name", numberOfLines: 0)
        var distanceLabel = UILabel(text: "Distance", textColor: .gray, textAlignment: .right)
        
        override var item: MKRoute.Step! {
            didSet {
                nameLabel.text = item.instructions
                
                var distance = ""
                
                
                if item.distance >= 1000 {
                    let kilometresConvertion = item.distance * 0.001
                    distance = String(format: "%.2f km", kilometresConvertion)
                } else {
                    distance = String(format: "%.2f m", item.distance)
                }
                
                distanceLabel.text = distance
            }
        }
        
        override func setupViews() {
            hstack(nameLabel, distanceLabel.withWidth(80)).withMargins(.allSides(16))
            
            addSeparatorView(leadingAnchor: nameLabel.leadingAnchor)
        }
    }
    
    
    class RoutesController: LBTAListHeaderController<RoutesStepCell, MKRoute.Step, RouteHeader>, UICollectionViewDelegateFlowLayout {
        override func viewDidLoad() {
            super.viewDidLoad()
            view.backgroundColor = .cyan
        }
        
        var route: MKRoute!
        
        override func setupHeader(_ header: RouteHeader) {
            header.setupHeaderInformation(route: route)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            .init(width: view.frame.width, height: 120)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return .init(width: view.frame.width, height: 70)
        }
    }
    
    var currentlyShowingRoute: MKRoute?
    
    fileprivate func setupStartEndDummyAnnotations() {
        let startAnnotation = MKPointAnnotation()
        startAnnotation.coordinate = .init(latitude: 37.766610, longitude: -122.427290)
        startAnnotation.title = "Start"
        
        let endAnnotation = MKPointAnnotation()
        endAnnotation.coordinate = .init(latitude: 37.331352, longitude: -122.030331)
        endAnnotation.title = "End"
        
        mapView.addAnnotation(startAnnotation)
        mapView.addAnnotation(endAnnotation)
        
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    fileprivate func requestForDirections() {
        let request = MKDirections.Request()
        
        request.source = startMapItem
        
        request.destination = endMapItem
        
//        request.requestsAlternateRoutes = true
        let directions = MKDirections(request: request)
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Routing..."
        
        hud.show(in: view)
        
        directions.calculate { (resp, err) in
            hud.dismiss()
            if let err = err {
                print("Failed to find routing info: ", err)
                return
            }
            
//            resp?.routes.forEach({ (route) in
//                self.mapView.addOverlay(route.polyline)
//            })
            
            if let route = resp?.routes.first {
                self.mapView.addOverlay(route.polyline)
            }
            
            self.currentlyShowingRoute = resp?.routes.first
            
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = #colorLiteral(red: 0.1053357795, green: 0.5102055669, blue: 0.848751843, alpha: 1)
        polylineRenderer.lineWidth = 5
        
        
        return polylineRenderer
    }
    
    let startTextField = IndentedTextField(padding: 12, cornerRadius: 5)
    let endTextField = IndentedTextField(padding: 12, cornerRadius: 5)
    
    fileprivate func setupNavBar() {
        view.addSubview(navBar)
        navBar.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: -120, right: 0))
        navBar.setupShadow(opacity: 0.5, radius: 5)
        
        let containerView = UIView(backgroundColor: .clear)
        
        navBar.addSubview(containerView)
        containerView.fillSuperviewSafeAreaLayoutGuide()
        
        [startTextField, endTextField].forEach { (tf) in
            tf.backgroundColor = .init(white: 1, alpha: 0.3)
            tf.textColor = .white
        }
        
        let startIcon = UIImageView(image: UIImage(named: "start_location_circles"), contentMode: .scaleAspectFit)
        startIcon.constrainWidth(20)
        
        let endIcon = UIImageView(image: UIImage(named: "annotation_icon")?.withRenderingMode(.alwaysTemplate), contentMode: .scaleAspectFit)
        endIcon.constrainWidth(20)
        endIcon.tintColor = .white
        
        startTextField.attributedPlaceholder = .init(string: "Start", attributes: [.foregroundColor: UIColor.init(white: 1, alpha: 0.7)])
        endTextField.attributedPlaceholder = .init(string: "End", attributes: [.foregroundColor: UIColor.init(white: 1, alpha: 0.7)])
        
        containerView.stack(
            containerView.hstack(startIcon, startTextField, spacing: 16),
            containerView.hstack(endIcon, endTextField, spacing: 16),
            spacing: 12, distribution: .fillEqually).withMargins(.init(top: 0, left: 16, bottom: 12, right: 16))
        
        startTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleChangeStartLocation)))
        endTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleChangeEndLocation)))
    }
    
    var startMapItem: MKMapItem?
    var endMapItem: MKMapItem?
    
    fileprivate func refreshMap() {
        mapView.removeAnnotations(mapView.annotations)
        mapView.showsUserLocation = false
        if let mapItem = startMapItem {
            let annotation = MKPointAnnotation()
            annotation.coordinate = mapItem.placemark.coordinate
            annotation.title = mapItem.name
            mapView.addAnnotation(annotation)
        }
        
        if let mapItem = endMapItem {
            let annotation = MKPointAnnotation()
            annotation.coordinate = mapItem.placemark.coordinate
            annotation.title = mapItem.name
            mapView.addAnnotation(annotation)
        }
        
        requestForDirections()
        
        mapView.showAnnotations(mapView.annotations, animated: true)
        mapView.showsUserLocation = true
    }
    
    @objc fileprivate func handleChangeStartLocation() {
        let vc = LocalSearchController()
        
        vc.selectionHandler = { [weak self] mapItem in
            self?.startTextField.text = mapItem.name
            self?.startMapItem = mapItem
            self?.refreshMap()
        }
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc fileprivate func handleChangeEndLocation() {
        let vc = LocalSearchController()
        
        vc.selectionHandler = { [weak self] mapItem in
            self?.endTextField.text = mapItem.name
            self?.endMapItem = mapItem
            self?.refreshMap()
        }
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func setupRegionForMap() {
        
        let centerCoordinate = CLLocationCoordinate2D(latitude: 37.766610, longitude: -122.427290)
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: centerCoordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}

extension DirectionsController: MKMapViewDelegate {
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//
//        if annotation is MKPointAnnotation {
//            let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "id")
//            annotationView.canShowCallout = true
//
//            return annotationView
//        }
//        return nil
//    }
}

struct DirectionsPreview: PreviewProvider {
    static var previews: some View {
        ContainerView().ignoresSafeArea(.all)
    }
    
    struct ContainerView: UIViewControllerRepresentable {
        func makeUIViewController(context: Context) -> some UIViewController {
            return UINavigationController(rootViewController: DirectionsController())
        }
        
        func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
            
        }
    }
}
