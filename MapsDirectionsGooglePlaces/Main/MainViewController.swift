//
//  MainViewController.swift
//  MapsDirectionsGooglePlaces
//
//  Created by Tri Nguyen on 16/01/2021.
//

import UIKit
import MapKit
import LBTATools
import Combine

class MainViewController: UIViewController {
    let mapView = MKMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(mapView)
        mapView.fillSuperview()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        requestUserLocation()
        
        setupRegionForMap()
        
        performLocalSearch()
        
        setupSearchUI()
        
        setupLocationsCarousel()
    }
    
    let locattionsController = LocationsCarouselController(scrollDirection: .horizontal)
    
    let locationManager = CLLocationManager()
    
    fileprivate func requestUserLocation() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .authorizedWhenInUse:
            print("Received authorization of user location")
            locationManager.startUpdatingLocation()
        default:
            print("Failed to authorize")
        }
    }
    
    fileprivate func setupLocationsCarousel() {
        let locationsView = locattionsController.view!
        
        view.addSubview(locationsView)
        locationsView.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: view.frame.width, height: 100))
        
        locattionsController.didSelectedItem = { [weak self] item in
            guard let self = self else { return }
            self.mapView.annotations.forEach { (annotation) in
                guard let customAnnotation = annotation as? MainViewController.CustomMKPointAnnotation else { return }
                if customAnnotation.mapItem === item {
                    self.mapView.selectAnnotation(customAnnotation, animated: true)
                }
            }
        }
    }
    
    let searchTextField = UITextField(placeholder: "Search query")
    
    var listener: AnyCancellable!
    
    fileprivate func setupSearchUI() {
        let whiteContainer = UIView(backgroundColor: .white)
        view.addSubview(whiteContainer)
        whiteContainer.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        
        whiteContainer.stack(searchTextField).withMargins(.allSides(16))
        
        listener = NotificationCenter.default.publisher(for: UITextField.textDidChangeNotification, object: searchTextField)
            .debounce(for: .milliseconds(500), scheduler: RunLoop.main)
            .sink { _ in
                self.performLocalSearch()
            }
    }
    
    @objc fileprivate func handleSearchChanges() {
        performLocalSearch()
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let customAnnotation = view.annotation as? CustomMKPointAnnotation else { return }
        
        guard let index = locattionsController.items.firstIndex(where: {$0.name == customAnnotation.mapItem?.name}) else { return }
        locattionsController.collectionView.scrollToItem(at: [0, index], at: .centeredHorizontally, animated: true)
        
    }
    
    fileprivate func performLocalSearch() {
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchTextField.text
        request.region = mapView.region
        let localSearch = MKLocalSearch(request: request)
        localSearch.start { (resp, err) in
            if let err = err {
                print("Failed local search:", err)
                return
            }
            
            //Success
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.locattionsController.items.removeAll()
            
            resp?.mapItems.forEach({ (mapItem) in
                
                print(mapItem.address())
                
                let annotation = CustomMKPointAnnotation()
                annotation.mapItem = mapItem
                annotation.coordinate = mapItem.placemark.coordinate
                annotation.title = "Location: \(mapItem.name ?? "")"
                self.mapView.addAnnotation(annotation)
                
                self.locattionsController.items.append(mapItem)
            })
            
            self.locattionsController.collectionView.scrollToItem(at: [0,0], at: .centeredHorizontally, animated: true)

            self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        }
    }
    
    class CustomMKPointAnnotation: MKPointAnnotation {
        var mapItem: MKMapItem?
    }
    
    fileprivate func setupAnnotationsForMap() {
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = .init(latitude: 37.766610, longitude: -122.427290)
        annotation.title = "San Fransico"
        annotation.subtitle = "CA"
        mapView.addAnnotation(annotation)
        
        let appleCampusAnnotation = MKPointAnnotation()
        appleCampusAnnotation.coordinate = .init(latitude: 37.332693, longitude: -122.030024)
        appleCampusAnnotation.title = "Apple Campus"
        appleCampusAnnotation.subtitle = "Cupertino, CA"
        mapView.addAnnotation(appleCampusAnnotation)
        
        mapView.showAnnotations(self.mapView.annotations, animated: true)
        
    }
    
    fileprivate func setupRegionForMap() {
        
        let centerCoordinate = CLLocationCoordinate2D(latitude: 37.766610, longitude: -122.427290)
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: centerCoordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}

extension MainViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let firstLocation = locations.first else { return }
        
        mapView.setRegion(.init(center: firstLocation.coordinate, span: .init(latitudeDelta: 0.1, longitudeDelta: 0.1)), animated: false)
        
//        locationManager.stopUpdatingLocation() 
    }
}

extension MainViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKPointAnnotation {
            let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "id")
            annotationView.canShowCallout = true
            
            return annotationView
        }
        return nil
    }
}

// MARK: - Preview SwiftUI

//import SwiftUI
//
//struct MainPreview: PreviewProvider {
//    static var previews: some View {
//        ContainerView().ignoresSafeArea(.all)
//    }
//
//    struct ContainerView: UIViewControllerRepresentable {
//        func makeUIViewController(context: Context) -> MainViewController {
//            return MainViewController()
//        }
//
//        func updateUIViewController(_ uiViewController: MainViewController, context: Context) {
//
//        }
//
//
//        typealias UIViewControllerType = MainViewController
//    }
//}
