//
//  DirectionsSearchView.swift
//  MapsDirectionsGooglePlaces
//
//  Created by Tri Nguyen on 09/02/2021.
//

import SwiftUI
import MapKit

struct DirectionsMapView: UIViewRepresentable {
    
    @EnvironmentObject var env: DirectionsEnvironment
    
    typealias UIViewType = MKMapView
    
    let mapView = MKMapView()
    
    func makeCoordinator() -> DirectionsMapView.Coordinator {
        return Coordinator(mapView: mapView)
    }
    
    class Coordinator: NSObject, MKMapViewDelegate {
        
        init(mapView: MKMapView) {
            super.init()
            mapView.delegate = self
        }
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = .red
            renderer.lineWidth = 5
            return renderer
        }
        
    }
    
    func makeUIView(context: Context) -> MKMapView {
        
        return mapView
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        uiView.removeOverlays(uiView.overlays)
        uiView.removeAnnotations(uiView.annotations)
        
        [env.sourceMapItem, env.destinationMapItem].compactMap{$0}.forEach { (mapItem) in
            let annotation = MKPointAnnotation()
            annotation.title = mapItem.name
            annotation.coordinate = mapItem.placemark.coordinate
            uiView.addAnnotation(annotation)
        }
        uiView.showAnnotations(uiView.annotations, animated: false)
        
        if let route = env.route {
            uiView.addOverlay(route.polyline)
        }
    }
    
}

struct SelectLocationView: View {
    
    @State var mapItems = [MKMapItem]()
    @State var searchQuery = ""
    @EnvironmentObject var env: DirectionsEnvironment
    
    var body: some View {
        VStack {
            HStack(spacing: 16) {
                Button(action: {
                    env.isSelectingSource = false
                    env.isSelectingDestination = false
                }, label: {
                    Image(uiImage: #imageLiteral(resourceName: "back_arrow"))
                }).foregroundColor(.black)
                
                TextField("Enter search terms", text: $searchQuery)
                    .onReceive(NotificationCenter.default.publisher(for: UITextField.textDidChangeNotification).debounce(for: .milliseconds(500), scheduler: RunLoop.main), perform: { _ in
                        let request = MKLocalSearch.Request()
                        request.naturalLanguageQuery = searchQuery
                        
                        let localSearch = MKLocalSearch(request: request)
                        localSearch.start { (resp, err) in
                            if let err = err {
                                print("Failed to local search:", err)
                                return
                            }
                            
                            self.mapItems = resp?.mapItems ?? []
                        }
                    })
                
            }.padding()
            
            
            ScrollView {
                ForEach(mapItems, id: \.self) { item in
                    Button(action: {
                        if self.env.isSelectingSource {
                            self.env.isSelectingSource = false
                            self.env.sourceMapItem = item
                        } else {
                            self.env.isSelectingDestination = false
                            self.env.destinationMapItem = item
                        }
                    }, label: {
                        HStack {
                            VStack(alignment: .leading) {
                                Text("\(item.name ?? "")").font(.headline)
                                Text("\(item.placemark.title ?? "")")
                            }
                            Spacer()
                        }
                        .padding()
                    }).foregroundColor(.black)
                }
            }
            
            Spacer()
        }
        .ignoresSafeArea(edges: .bottom)
        .onAppear(perform: {
            
        })
        .navigationBarHidden(true)
    }
}

struct DirectionsSearchView: View {
    
    @EnvironmentObject var env: DirectionsEnvironment
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .top) {
                VStack(spacing: 0) {
                    VStack(spacing: 12) {
                        MapItemView(isSelected: $env.isSelectingSource, title: env.sourceMapItem != nil ? (env.sourceMapItem?.name ?? "") : "Source", image: #imageLiteral(resourceName: "start_location_circles"))
                        MapItemView(isSelected: $env.isSelectingDestination, title: env.destinationMapItem != nil ? (env.destinationMapItem?.name ?? "") : "Destination", image: #imageLiteral(resourceName: "annotation_icon"))
                    }
                    .padding()
                    .background(Color.blue)
                    
                    DirectionsMapView().ignoresSafeArea(edges: .bottom)
                    
                }
                
                StatusBarCover()
                
                HUDView(isShowing: env.isCalculatingDirections)
                
            }
            .navigationBarHidden(true)
        }
        
    }
}

struct HUDView: View {
    var isShowing: Bool
    
    var body: some View {
        if isShowing {
            VStack {
                Spacer()
                VStack {
                    VStack {
                        LoadingHUD()
                        Text("Loading...")
                            .font(.headline)
                            .foregroundColor(.white)
                    }.padding()
                }
                .background(Color.black)
                .cornerRadius(5)
                Spacer()
            }
        }
    }
}

struct LoadingHUD: UIViewRepresentable {
    typealias UIViewType = UIActivityIndicatorView
    
    func makeUIView(context: Context) -> UIActivityIndicatorView {
        let aiv = UIActivityIndicatorView(style: .large)
        aiv.color = .white
        aiv.startAnimating()
        return aiv
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: Context) {
        
    }
}

struct MapItemView: View {
    
    @EnvironmentObject var env: DirectionsEnvironment
    
    @Binding var isSelected: Bool
    
    var title: String
    
    var image: UIImage
    
    var body: some View {
        HStack(spacing: 16) {
            Image(uiImage: image.withRenderingMode(.alwaysTemplate))
                .frame(width: 24).foregroundColor(.white)
            NavigationLink(
                destination: SelectLocationView(),
                isActive: $isSelected,
                label: {
                    HStack {
                        Text(title)
                        Spacer()
                    }
                    .padding()
                    .background(Color.white)
                    .cornerRadius(3)
                }
            )
        }
    }
}

struct StatusBarCover: View {
    var body: some View {
        Spacer().frame(width: UIApplication.shared.windows.filter{$0.isKeyWindow}.first?.frame.width, height: UIApplication.shared.windows.filter{$0.isKeyWindow}.first?.safeAreaInsets.top).background(Color.blue)
            .ignoresSafeArea(edges: .top)
    }
}

import Combine

class DirectionsEnvironment: ObservableObject {
    @Published var isSelectingSource = false
    @Published var isSelectingDestination = false
    @Published var sourceMapItem: MKMapItem?
    @Published var destinationMapItem: MKMapItem?
    
    @Published var route: MKRoute?
    
    @Published var isCalculatingDirections = false
    
    var cancellable: AnyCancellable?
    
    init() {
        cancellable = Publishers.CombineLatest($sourceMapItem, $destinationMapItem).sink(receiveValue: { [weak self] (items) in
            let request = MKDirections.Request()
            request.source = items.0
            request.destination = items.1
            let direction = MKDirections(request: request)
            self?.isCalculatingDirections = true
            self?.route = nil
            direction.calculate { (resp, err) in
                self?.isCalculatingDirections = false
                if let err = err {
                    print("Failed to calculate directions:", err)
                    return
                }
                
                self?.route = resp?.routes.first
            }
        })
    }
}

struct DirectionsSearchView_Previews: PreviewProvider {
    static let env = DirectionsEnvironment()
    static var previews: some View {
        DirectionsSearchView().environmentObject(env)
    }
}
