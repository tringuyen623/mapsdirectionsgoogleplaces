//
//  MapSearchingViewModel.swift
//  MapsDirectionsGooglePlaces
//
//  Created by Tri Nguyen on 04/02/2021.
//

import UIKit
import MapKit
import Combine
import SwiftUI

class MapSearchingViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    @Published var annotations = [MKPointAnnotation]()
    @Published var searchQuery = ""
    @Published var mapItems = [MKMapItem]()
    @Published var selectedMapItem: MKMapItem?
    //    @Published var keyboardHeight: CGFloat = 0
    
    @Published var currentLocation = CLLocationCoordinate2D(latitude: 37.766610, longitude: -122.427290)
    
    let locationManager = CLLocationManager()
    
    var cancellable: AnyCancellable?
    
    var region: MKCoordinateRegion?
    
    override init() {
        super.init()
        cancellable = $searchQuery.debounce(for: .milliseconds(500), scheduler: RunLoop.main)
            .sink(receiveValue: { (queryTerm) in
                self.performSearch(query: queryTerm)
            })
        
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestWhenInUseAuthorization()
        
        //        listenForKeyboardNotifications() //SwiftUI 2.0 automatic handle keyboard show/hide with animation
        
        NotificationCenter.default.addObserver(forName: MapViewContainer.Coordinator.regionChangedNotification, object: nil, queue: .main) { [weak self] (notification) in
            self?.region = notification.object as? MKCoordinateRegion
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        let status = manager.authorizationStatus
        
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let firstLocation = locations.first else { return }
        currentLocation = firstLocation.coordinate
    }
    
    //    fileprivate func listenForKeyboardNotifications() {
    //        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main) { [weak self] (notification) in
    //            guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
    //            let keyboardFrame = value.cgRectValue
    //            let windown = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
    //            withAnimation(.easeOut(duration: 0.25)) {
    //                self?.keyboardHeight = keyboardFrame.height - (windown?.safeAreaInsets.bottom ?? 0)
    //            }
    //            print(keyboardFrame.height)
    //        }
    //
    //        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: .main) { [weak self] (notification) in
    //            withAnimation(.easeOut(duration: 0.25)) {
    //                self?.keyboardHeight = 0
    //            }
    //
    //        }
    //    }
    
    func performSearch(query: String) {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = query
        
        if let region = self.region {
            request.region = region
        }
        
        let localSearch = MKLocalSearch(request: request)
        
        localSearch.start { (resp, err) in
            if let err = err {
                print("Failed to search:", err)
                return
            }
            
            self.mapItems = resp?.mapItems ?? []
            
            var airportAnnotations = [MKPointAnnotation]()
            
            resp?.mapItems.forEach({ (mapItem) in
                let annotation = MKPointAnnotation()
                annotation.title = mapItem.name
                annotation.coordinate = mapItem.placemark.coordinate
                
                airportAnnotations.append(annotation)
            })
            
            self.annotations = airportAnnotations
        }
    }
}
