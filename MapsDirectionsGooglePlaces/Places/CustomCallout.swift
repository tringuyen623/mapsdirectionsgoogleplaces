//
//  CustomCallout.swift
//  MapsDirectionsGooglePlaces
//
//  Created by Tri Nguyen on 02/02/2021.
//

import UIKit

class CalloutContainer: UIView {
    
    let imageView = UIImageView(image: nil, contentMode: .scaleAspectFill)
    let nameLabel = UILabel(textAlignment: .center)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        
        setupSpinerView()
        
        setupSubView()
    }
    
    fileprivate func setupView() {
        backgroundColor = .white
        
        layer.borderWidth = 2
        layer.borderColor = UIColor.darkGray.cgColor
        
        setupShadow(opacity: 0.2, radius: 5, offset: .zero, color: .darkGray)
        layer.cornerRadius = 5
    }
    
    fileprivate func setupSubView() {
        addSubview(imageView)
        imageView.layer.cornerRadius = 5
        imageView.fillSuperview()
        
        let labelContainer = UIView(backgroundColor: .white)
        labelContainer.stack(nameLabel)
        stack(UIView(), labelContainer.withHeight(30))
    }
    
    fileprivate func setupSpinerView() {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.color = .darkGray
        addSubview(spinner)
        spinner.fillSuperview()
        spinner.startAnimating()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
