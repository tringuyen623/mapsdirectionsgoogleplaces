//
//  PlacesController.swift
//  MapsDirectionsGooglePlaces
//
//  Created by Tri Nguyen on 02/02/2021.
//

import SwiftUI
import UIKit
import LBTATools
import MapKit
import GooglePlaces
import JGProgressHUD

class PlacesController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    let mapView = MKMapView()
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(mapView)
        mapView.fillSuperview()
        
        mapView.delegate = self
        locationManager.delegate = self
        mapView.showsUserLocation = true
        
        requestForLocationAuthorization()
        
//        setupSelectedAnnotaionHUD()
    }
    
    let hudNameLabel = UILabel(text: "Name", font: .boldSystemFont(ofSize: 16))
    let hudAddressLabel = UILabel(text: "Address", font: .boldSystemFont(ofSize: 16))
    let hudTypesLabel = UILabel(text: "Types", textColor: .gray)
    lazy var infoButton = UIButton(type: .infoLight)
    let hudContainer = UIView(backgroundColor: .white)
    
    fileprivate func setupSelectedAnnotaionHUD() {
        
        infoButton.addTarget(self, action: #selector(handleInformation), for: .touchUpInside)
        
        view.addSubview(hudContainer)
        hudContainer.layer.cornerRadius = 5
        hudContainer.setupShadow(opacity: 0.2, radius: 5, offset: .zero, color: .darkGray)
        hudContainer.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .allSides(16))
        
        let topRow = UIView()
        topRow.hstack(hudNameLabel, infoButton.withWidth(44))
        hudContainer.hstack(hudContainer.stack(topRow, hudAddressLabel, hudTypesLabel, spacing: 8), alignment: .center).withMargins(.allSides(16))
    }
    
    @objc fileprivate func handleInformation() {
        guard let selectedAnnotation = mapView.selectedAnnotations.first as? PlaceAnnotation,
              let placeId = selectedAnnotation.place.placeID else { return }
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading photos..."
        hud.show(in: view)
        
        client.lookUpPhotos(forPlaceID: placeId) { [weak self] (photolist, err) in
            if let err = err {
                hud.dismiss()
                print("Failed to lookup photo for place:", err)
                return
            }
            
            let dispatchGroup = DispatchGroup()
            
            var images = [UIImage]()
            
            photolist?.results.forEach({ (photoMetadata) in
                dispatchGroup.enter()
                self?.client.loadPlacePhoto(photoMetadata) { (image, err) in
                    dispatchGroup.leave()
                    if let err = err {
                        hud.dismiss()
                        print("Failed to load photo for place: ", err)
                        return
                    }
                    
                    guard let image = image else { return }
                    images.append(image)
                }
                
                
            })
            
            dispatchGroup.notify(queue: .main) {
                hud.dismiss()
                let vc = PlacePhotosController()
                vc.items = images
                
                self?.present(UINavigationController(rootViewController: vc), animated: true)
            }
        }
        
    }
    
    let client = GMSPlacesClient()
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is PlaceAnnotation) { return nil }
        
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "id")
//        annotationView.canShowCallout = true
        
        if let placeAnnotation = annotation as? PlaceAnnotation {
            let types = placeAnnotation.place.types
            
            if let firstType = types?.first {
                switch firstType {
                case "bar":
                    annotationView.image = UIImage(named: "bar")
                    break
                case "restaurant":
                    annotationView.image = UIImage(named: "restaurant")
                    break
                case "tourist":
                    annotationView.image = UIImage(named: "tourist")
                    break
                default:
                    annotationView.image = UIImage(named: "default")
                }
            }
        }
        return annotationView
    }
    
    var currentCustomCallout: UIView?
    var customCalloutWidthConstraint: NSLayoutConstraint!
    var customCalloutHeightConstraint: NSLayoutConstraint!
    
    fileprivate func setupHUD(view: MKAnnotationView) {
        guard let annotation = view.annotation as? PlaceAnnotation else { return }
        
        let place = annotation.place
        hudNameLabel.text = place.name
        hudAddressLabel.text = place.formattedAddress
        hudTypesLabel.text = place.types?.joined(separator: ", ")
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        currentCustomCallout?.removeFromSuperview()
        
        setupSelectedAnnotaionHUD()
        
        setupHUD(view: view)
        
        let customCalloutContainer = CalloutContainer()
        
        view.addSubview(customCalloutContainer)
        
        customCalloutContainer.centerXToSuperview()
        customCalloutWidthConstraint = customCalloutContainer.widthAnchor.constraint(equalToConstant: 100)
        customCalloutWidthConstraint.isActive = true
        customCalloutHeightConstraint = customCalloutContainer.heightAnchor.constraint(equalToConstant: 200)
        customCalloutHeightConstraint.isActive = true
        customCalloutContainer.bottomAnchor.constraint(equalTo: view.topAnchor).isActive = true
        
        currentCustomCallout = customCalloutContainer
        
        customCalloutContainer.nameLabel.text = (view.annotation as? PlaceAnnotation)?.place.name
        loadPhotoInCustomCallout(view: view, calloutContainer: customCalloutContainer)
        
    }
    
    fileprivate func loadPhotoInCustomCallout(view: MKAnnotationView, calloutContainer: CalloutContainer) {
        
        guard let firstPhotoMetadata = (view.annotation as? PlaceAnnotation)?.place.photos?.first else {
            calloutContainer.removeFromSuperview()
            return
        }
        
        client.loadPlacePhoto(firstPhotoMetadata, callback: { [weak self] (image, err) in
            if let err = err {
                print("Failed to load photo for place : ", err)
            }
            
            guard let image = image else { return }
            
            guard let bestSize = self?.calculateCalloutImageSize(image: image) else { return }
            self?.customCalloutWidthConstraint.constant = bestSize.width
            self?.customCalloutHeightConstraint.constant = bestSize.height
            
            calloutContainer.imageView.image = image
            calloutContainer.nameLabel.text = (view.annotation as? PlaceAnnotation)?.place.name
        })
    }
    
    fileprivate func calculateCalloutImageSize(image: UIImage) -> CGSize {
        if image.size.width > image.size.height {
            let newWidth: CGFloat = 300
            let newHeight = newWidth / image.size.width * image.size.height
            return .init(width: newWidth, height: newHeight)
        } else {
            let newHeight: CGFloat = 200
            let newWidth = image.size.width / image.size.height * newHeight
            return .init(width: newWidth, height: newHeight)
        }
    }
    
    fileprivate func findNearbyPlaces() {
        client.currentPlace { [weak self] (likelihoodList, err) in
            
            guard let self = self else { return }
            if let err = err {
                print("Faild to find current places: ", err)
                return
            }
            
            likelihoodList?.likelihoods.forEach({ (likehood) in
                let place = likehood.place
                
                let annotation = PlaceAnnotation(place: place)
                annotation.title = place.name
                annotation.coordinate = place.coordinate
                
                self.mapView.addAnnotation(annotation)
            })
            
            self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        }
    }
    
    class PlaceAnnotation: MKPointAnnotation {
        let place: GMSPlace
        
        init(place: GMSPlace) {
            self.place = place
        }
    }
    
    fileprivate func requestForLocationAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let firtLocation = locations.first else { return }
        
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        
        let region = MKCoordinateRegion(center: firtLocation.coordinate, span: span)
        
        mapView.setRegion(region, animated: false)
        
        findNearbyPlaces()
    }
    
}

struct PlacesController_Previews: PreviewProvider {
    static var previews: some View {
        Container().ignoresSafeArea()
    }
    
    struct Container: UIViewControllerRepresentable {
        func makeUIViewController(context: Context) -> some UIViewController {
            PlacesController()
        }
        
        func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
            
        }
    }
}
