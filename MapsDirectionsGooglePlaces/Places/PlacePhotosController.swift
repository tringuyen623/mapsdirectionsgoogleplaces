//
//  PlacePhotosController.swift
//  MapsDirectionsGooglePlaces
//
//  Created by Tri Nguyen on 03/02/2021.
//

import UIKit
import LBTATools

class PhotoCell: LBTAListCell<UIImage> {
    let imageView = UIImageView(image: nil, contentMode: .scaleAspectFill)
    
    override var item: UIImage! {
        didSet {
            imageView.image = item
        }
    }
    
    override func setupViews() {
        addSubview(imageView)
        imageView.fillSuperview()
    }
}


class PlacePhotosController: LBTAListController<PhotoCell, UIImage>, UICollectionViewDelegateFlowLayout {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Photos"
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.height, height: 300)
    }
}
